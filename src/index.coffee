# packages
TimeoutAgent = require '@dark-hole/timeout-agent'
Keys         = require '@dark-hole/keys'

# environment variables
GATE_URI            = process.env.GATE_URI
AGENT_NAME          = process.env.AGENT_NAME
TIMEOUT             = process.env.TIMEOUT || 2
DOCKER_COMPOSE_FILE = process.env.DOCKER_COMPOSE_FILE
AGENT_KEYS_FILE     = process.env.AGENT_KEYS_FILE || '/agent/.keys'
AGENT_EXEC_FILE     = process.env.AGENT_EXEC_FILE || '/agent/exec'

# throw bad construction
throw new Error 'missing environment variables' if not GATE_URI or not AGENT_NAME

# load or generate keys
try
  keys = Keys.load AGENT_KEYS_FILE
catch e
  keys = new Keys b : 512
  keys.save AGENT_KEYS_FILE

# require exec code
_exec = require AGENT_EXEC_FILE

agent = new TimeoutAgent GATE_URI,
  name    : AGENT_NAME
  keys    : keys
  timeout : TIMEOUT
  compose : DOCKER_COMPOSE_FILE
  action  : _exec

agent.setup()

