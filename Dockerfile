FROM node:9-alpine

# prepare scenario
ENV PATH="node_modules/.bin:${PATH}"
RUN mkdir /app /agent

# copy source code
COPY ./src /code/src
COPY ./package.json /code/

# build project
WORKDIR /code
RUN npm install --only=dev
RUN npm run build

# copy dist
RUN cp -r dist /app
RUN cp package.json /app

# install prod
WORKDIR /app
RUN npm install --only=prod

# clean
RUN rm -rf /code

# default pwd
WORKDIR /agent

# run command
CMD [ "node", "/app/dist" ]

